<?php declare(strict_types=1);

namespace OmekaTheme\Helper;

require_once __DIR__ . '/ThemeFunctionsRights.php';

use Omeka\Api\Representation\ValueRepresentation;

trait ThemeFunctionsSpecific
{
    use ThemeFunctionsRights;

    /**
     * Convertit une value en html.
     *
     * @todo Le AsHtml() devrait suffire avec les événements.
     * @deprecated Utiliser les filtres values ou le partial resource-values.
     */
    public function simpleValueAsHtmlForTerm(ValueRepresentation $value, string $term, ?array $linkables = null): string
    {
        $val = $this->simpleValueForTerm($value, $term);
        return $val['link'] ?? (string) $val['value'];
    }

    /**
     * Convertit une value en valeur simple pour le html.
     *
     * @todo Check and use CleanUrl.
     * @deprecated Use a filter on value.
     */
    public function simpleValueForTerm(ValueRepresentation $value, string $term, ?array $linkables = null): array
    {
        // Manage AdvancedResourceTemplate terms with complex groups.
        //  TODO Check if it is still needed with the append iterator.
        $mainTerm = strtok($term, '/');

        /** @var \Omeka\Api\Representation\ValueRepresentation $value*/
        $dataType = $value->type();
        $val = [
            'v' => $value,
            'class' => 'value',
            'lang' => $value->lang(),
            'value' => null,
            'url' => null,
            'link' => null,
        ];

        // Une ressource peut être gérée via custom vocab: son type n’est pas seulement "resource"…
        $valueResource = $value->valueResource();
        if ($valueResource) {
            $val['class'] .= ' resource ' . $valueResource->resourceName();
            $val['link'] = $valueResource->link($valueResource->displayTitle());
        } elseif ($uri = (string) $value->uri()) {
            // TODO Le AsHtml() devrait suffire avec les événements.
            $vv = (string) $value->value();
            // Pas d’autres classes pour les autres types de données.
            $val['class'] .= ' uri';
            $val['url'] = $uri;
            // Value suggest, rights statement, etc.
            if (strlen($vv)) {
                // Lien de recherche interne.
                return $this->browseValueForTerm($value, $mainTerm);
            } else {
                // Lien externe en l’absence de label, car sinon ce n’est pas clair.
                $val['link'] = str_replace('<a ', '<a _target="blank" ', $value->asHtml());
            }
        } else {
            // Pas d’autres classes pour les autres types de données.
            // TODO Le AsHtml() devrait suffire avec les événements.
            $vv = (string) $value->value();
            if (
                // Toujours liens.
                (is_null($linkables)
                    || in_array($term, $linkables)
                    || substr($dataType, 0, 11) === 'customvocab'
                    )
                // Jamais liens.
                && !(substr($dataType, 0, 7) === 'numeric'
                    || in_array($dataType, [
                        'numeric:timestamp',
                        'numeric:integer',
                        'numeric:interval',
                        'numeric:duration',
                        'html',
                        'xml',
                        'boolean',
                        'geography',
                        'geometry',
                        'geography:coordinates',
                        'geometry:coordinates',
                        'geometry:position',
                    ])
                )
            ) {
                // Lien de recherche interne.
                return $this->browseValueForTerm($value, $mainTerm);
            } else {
                // $val['value'] = $vv;
                $val['value'] = $value->asHtml();
            }
        }

        if (!$value->isPublic()) {
            $val['class'] .= ' private';
        }

        return $val;
    }
}
