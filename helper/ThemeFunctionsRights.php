<?php declare(strict_types=1);

namespace OmekaTheme\Helper;

use Omeka\Api\Representation\AbstractResourceEntityRepresentation;

trait ThemeFunctionsRights
{
    /**
     * Get the standard Rights Statements code from a uri (recommended) or a code.
     *
     * Note: the official uris use scheme "http:", not "https:". For a simpler
     * web browsing, the https is used in the output (url, not uri).
     *
     * @link https://rightsstatements.org/en/documentation/
     * @link https://rightsstatements.org/en/statements/
     * @link https://rightsstatements.org/en/documentation/usage_guidelines.html
     *
     * This is a generic theme replacement to the module RightsStatements.
     * Main differences are the possibility to query on uri, the use of codes
     * and the distinction between uri and url.
     *
     * @link https://github.com/zerocrates/RightsStatements
     *
     * @see https://bibnum.campus-condorcet.fr/
     * @see https://collections.maison-salins.fr/
     */
    public function rightsStatements(
        ?AbstractResourceEntityRepresentation $resource,
        ?string $term = 'dcterms:rights',
        ?string $codeOrUrl = null,
        ?string $output = null,
        array $options = []
    ) {
        // This table is available as rdf.
        // Other codes are not standard and not supported.
        $data = [
            'InC' => [
                'code' => 'InC',
                'url' => 'https://rightsstatements.org/vocab/InC/1.0/',
                'label' => 'In Copyright',
                'title' => 'Protégé par le droit d’auteur',
                'summary' => 'La présente Déclaration des Droits peut être utilisée pour un Objet protégé par le droit d’auteur. L’utilisation de la présente Déclaration implique que l’organisme qui rend l’Objet accessible ait constaté que celui-ci est protégé par le droit d’auteur et que, soit il est le titulaire des droits sur l’Objet, soit il a obtenu du/des titulaire(s) des droits l’autorisation de rendre accessible(s) son/ses/leur(s) œuvre(s) ou alors il rend l’Objet accessible en vertu d’une exception ou d’une limitation au droit d’auteur (y compris le “Fair use”) qui l’autorise à rendre l’Objet accessible.',
            ],
            'InC-RUU' => [
                'code' => 'InC-RUU',
                'url' => 'https://rightsstatements.org/vocab/InC-RUU/1.0/',
                'label' => 'In Copyright - Rights-holder(s) Unlocatable or Unidentifiable',
                'title' => 'Protégé par le droit d’auteur - Titulaire(s) des droits impossible(s) à localiser ou à identifier',
                'summary' => 'La présente Déclaration des Droits est destinée à être utilisée pour un Objet identifié comme étant protégé par le droit d’auteur, mais pour lequel aucun titulaire des droits n’a pu être identifié ou localisé au terme de recherches raisonnables. La présente Déclaration des Droits ne doit être utilisée que si l’organisme envisageant de rendre l’Objet accessible a la certitude raisonnable que l’Œuvre sous-jacente est protégée par le droit d’auteur. Cette Déclaration des Droits n’est pas destinée à être utilisée par les organismes basés dans l’UE qui ont identifié des Œuvres comme étant des Œuvres Orphelines conformément à la Directive Européenne sur les Œuvres Orphelines (elles doivent plutôt utiliser la Déclaration InC-OW-EU).',
            ],
            'InC-NC' => [
                'code' => 'InC-NC',
                'url' => 'https://rightsstatements.org/vocab/InC-NC/1.0/',
                'label' => 'In Copyright - Non-Commercial Use Permitted',
                'title' => 'Protégé par le droit d’auteur - Utilisation non commerciale autorisée',
                'summary' => 'La présente Déclaration des Droits peut uniquement être utilisée pour les Objets protégés par le droit d’auteur. De plus, l’organisme qui les rend accessibles doit être titulaire des droits ou avoir été explicitement autorisé par le(s) titulaire(s) des droits à permettre à des tiers d’utiliser son/ses/leur(s) œuvre(s) à des fins pédagogiques sans autorisation préalable.',
            ],
            'InC-EDU' => [
                'code' => 'InC-EDU',
                'url' => 'https://rightsstatements.org/vocab/InC-EDU/1.0/',
                'label' => 'In Copyright - Educational Use Permitted',
                'title' => 'Protégé par le droit d’auteur - Utilisation à des fins pédagogiques autorisée',
                'summary' => 'La présente Déclaration des Droits peut uniquement être utilisée pour les Objets protégés par le droit d’auteur. De plus, l’organisme qui les rend accessibles doit être titulaire des droits ou avoir été explicitement autorisé par le(s) titulaire(s) des droits à permettre à des tiers d’utiliser son/ses/leur(s) œuvre(s) à des fins pédagogiques sans autorisation préalable.',
            ],
            'InC-OW-EU' => [
                'code' => 'InC-OW-EU',
                'url' => 'https://rightsstatements.org/vocab/InC-OW-EU/1.0/',
                'label' => 'In Copyright - EU Orphan Work',
                'title' => 'Protégé par le droit d’auteur - Œuvre Orpheline de l’UE',
                'summary' => 'La présente Déclaration des Droits est destinée à être utilisée pour les Objets dont l’œuvre sous-jacente a été identifiée comme Œuvre Orpheline, conformément à la Directive 2012/28/UE du Parlement Européen et du Conseil du 25 octobre 2012 sur certaines utilisations autorisées des Œuvres Orphelines. Elle ne peut être utilisée que pour des Objets dérivés d’Œuvres couvertes par la Directive : il peut s’agir d’Œuvres publiées sous forme de livres, de revues, de journaux, de magazines ou d’autres écrits, d’œuvres cinématographiques ou audiovisuelles et de phonogrammes (attention : la photographie et les arts visuels sont exclus). Cette Déclaration peut uniquement être utilisée par des organismes bénéficiaires de la Directive : les bibliothèques, les établissements d’enseignement et les musées accessibles au public, les archives, les institutions dépositaires du patrimoine cinématographique ou sonore et les organismes de radiodiffusion de service public, établis dans l’un des États membres de l’UE. En outre, le bénéficiaire doit avoir enregistré l’Œuvre dans la Base de données des Œuvres Orphelines de l’UE gérée par l’EUIPO.',
            ],
            'NoC-OKLR' => [
                'code' => 'NoC-OKLR',
                'url' => 'https://rightsstatements.org/vocab/NoC-OKLR/1.0/',
                'label' => 'No Copyright - Other Known Legal Restrictions',
                'title' => 'Absence de protection par le droit d’auteur - Autres restrictions juridiques connues',
                'summary' => 'Cette Déclaration des Droits devrait être utilisée pour les Objets qui sont dans le Domaine Public, mais dont la libre réutilisation est soumise à des restrictions juridiques connues, liées par exemple à la protection du patrimoine culturel ou des expressions culturelles traditionnelles. Ainsi, l’organisme qui entend rendre l’Objet accessible ne pourra pas autoriser sa libre réutilisation. Pour que la présente Déclaration des Droits soit probante, l’organisme qui entend rendre l’Objet accessible doit fournir un lien vers une page spécifiant les restrictions juridiques limitant la réutilisation de l’Objet.',
            ],
            'NoC-CR' => [
                'code' => 'NoC-CR',
                'url' => 'https://rightsstatements.org/vocab/NoC-CR/1.0/',
                'label' => 'No Copyright - Contractual Restrictions',
                'title' => 'Absence de protection par le droit d’auteur - Restrictions contractuelles',
                'summary' => 'L’utilisation de cette Déclaration des Droits est limitée aux objets qui sont dans le Domaine Public, mais pour lesquels l’organisme qui entend rendre l’Objet accessible a conclu une convention qui l’oblige à imposer des restrictions quant à l’utilisation de l’Objet par des tiers. Pour que la présente Déclaration des Droits soit probante, l’organisme qui entend rendre l’Objet accessible doit fournir un lien vers une page spécifiant les restrictions contractuelles applicables à l’utilisation de l’Objet.',
            ],
            'NoC-NC' => [
                'code' => 'NoC-NC',
                'url' => 'https://rightsstatements.org/vocab/NoC-NC/1.0/',
                'label' => 'No Copyright - Non-Commercial Use Only',
                'title' => 'Absence de protection par le droit d’auteur - Utilisation à des fins uniquement non commerciales',
                'summary' => 'L’utilisation de cette Déclaration des Droits est limitée aux œuvres qui sont dans le Domaine Public et qui ont été numérisées au sein d’un partenariat public-privé dans le cadre duquel les parties contractantes ont convenu de limiter les utilisations commerciales de cette représentation numérique de l’Œuvre par des tiers. Spécialement élaborée afin de pouvoir inclure les œuvres numérisées dans le cadre des partenariats existant entre des bibliothèques européennes et Google, elle peut théoriquement être utilisée pour des Objets qui ont été numérisés dans le cadre de partenariats public-privé similaires.',
            ],
            'NoC-US' => [
                'code' => 'NoC-US',
                'url' => 'https://rightsstatements.org/vocab/NoC-US/1.0/',
                'label' => 'No Copyright - United States',
                'title' => 'Absence de protection par le droit d’auteur - États-Unis',
                'summary' => 'Cette Déclaration des Droits devrait être utilisée pour les Objets pour lesquels l’organisme qui entend rendre l’Objet accessible a conclu qu’ils étaient libres de droits selon la législation des États-Unis. La présente Déclaration des Droits ne devrait pas être utilisée pour les Œuvres Orphelines (qui sont présumées être protégées par le droit d’auteur) ou pour les objets pour lesquels l’organisme qui entend rendre accessible l’Objet n’a pas pris de mesures pour déterminer le statut de l’œuvre sous-jacente au vu du droit d’auteur.',
            ],
            'NKC' => [
                'code' => 'NKC',
                'url' => 'https://rightsstatements.org/vocab/NKC/1.0/',
                'label' => 'No Known Copyright',
                'title' => 'Absence de protection connue par le droit d’auteur',
                'summary' => 'Cette Déclaration des Droits doit être utilisée pour les Objets dont le statut en matière de droit d’auteur n’a pas été déterminé de manière définitive, mais pour lesquels l’organisme qui entend rendre l’Objet accessible a des motifs raisonnables de croire que l’Œuvre sous-jacente n’est plus couverte par le droit d’auteur ou des droits voisins. La présente Déclaration des Droits ne doit pas être utilisée pour les Œuvres Orphelines (qui sont présumées être protégées par le droit d’auteur) ou pour les Objets pour lesquels l’organisme qui entend rendre accessible l’Objet n’a pas pris de mesures pour déterminer le statut de l’œuvre sous-jacente en ce qui concerne le droit d’auteur.',
            ],
            'UND' => [
                'code' => 'UND',
                'url' => 'https://rightsstatements.org/vocab/UND/1.0/',
                'label' => 'Copyright Undetermined',
                'title' => 'Droit d’auteur indéterminé',
                'summary' => 'Cette Déclaration des Droits doit être utilisée pour les Objets dont le statut en matière de droit d’auteur est inconnu et pour lesquels l’organisme qui a rendu l’Objet accessible a pris des mesures (infructueuses) pour déterminer le statut de l’Œuvre sous-jacente au vu du droit d’auteur. En règle générale, cette Déclaration des Droits est utilisée lorsque l’organisme n’a pas connaissance de tous les éléments-clés nécessaires pour déterminer avec précision le statut de l’Objet en matière de droit d’auteur.',
            ],
            'CNE' => [
                'code' => 'CNE',
                'url' => 'https://rightsstatements.org/vocab/CNE/1.0/',
                'label' => 'Copyright Not Evaluated',
                'title' => 'Droit d’auteur non évalué',
                'summary' => 'Cette Déclaration des Droits devrait être utilisée pour les Objets dont le statut en matière de droit d’auteur est inconnu et pour lesquels l’organisme qui entend rendre l’Objet accessible n’a pas pris de mesures pour déterminer le statut de l’œuvre sous-jacente au vu du droit d’auteur.',
            ],
        ];

        $codes2Urls = array_column($data, 'url', 'code');
        $codes2Labels = array_column($data, 'label', 'code');
        $lowerCodes = array_change_key_case(array_combine(array_keys($codes2Urls), array_keys($codes2Urls)));

        $url = null;
        $code = null;
        $label = null;

        $checkCode = function ($input) use ($codes2Urls, $codes2Labels, $lowerCodes): ?string {
            $input = (string) $input;
            if (!strlen($input)) {
                return null;
            }
            $input = strtok($input, '?');
            $lowerInput = strtolower($input);
            if (isset($lowerCodes[$lowerInput])) {
                return $lowerCodes[$lowerInput];
            }
            $inputFix = str_replace(['https:', 'page', 'data'], ['http:', 'vocab', 'vocab'], $input);
            $code = array_search($inputFix, $codes2Urls);
            if ($code) {
                return $code;
            }
            $code = array_search($input, $codes2Labels);
            if ($code) {
                return $code;
            }
            return null;
        };

        if ($resource) {
            foreach ($resource->value($term, ['all' => true]) as $value) {
                $url = strtolower((string) $value->uri());
                $label = trim((string) $value->value());
                $code = null;
                if ($url) {
                    $url = str_replace(['http:', 'data', 'page'], ['https:', 'vocab', 'vocab'], $url);
                    if (substr($url, 0, 29) === 'https://rightsstatements.org/') {
                        break;
                    }
                    continue;
                }
                $labelFix = str_replace(['http:', 'data', 'page'], ['https:', 'vocab', 'vocab'], $label);
                if (substr($labelFix, 0, 29) === 'https://rightsstatements.org/') {
                    $url = $labelFix;
                    $label = null;
                    break;
                }
                $code = $checkCode($label);
                if ($code) {
                    break;
                }
            }
            if (empty($url) && empty($code)) {
                return null;
            }
        } elseif ($codeOrUrl) {
            $codeOrUrl = str_replace(['http:', 'data', 'page'], ['https:', 'vocab', 'vocab'], trim(strtok($codeOrUrl, '?')));
            if (substr($codeOrUrl, 0, 29) === 'https://rightsstatements.org/') {
                $url = $codeOrUrl;
            } else {
                $code = $checkCode($codeOrUrl);
                if (!$code) {
                    return null;
                }
            }
        } else {
            return null;
        }

        if ($url && $code) {
            // Nothing to do.
        } elseif ($url && empty($code)) {
            $regex = '~^https?://rightsstatements\.org/\w+/(inc-ow-eu|inc-edu|inc-nc|inc-ruu|inc|noc-cr|noc-nc|noc-oklr|noc-us|cne|und|nkc)(?:/?|/\d\.\d/?)$~';
            $code = preg_replace($regex, '$1', strtolower(strtok($url, '?')));
            if (!isset($lowerCodes[$code])) {
                return null;
            }
            $code = $lowerCodes[$code];
            $url = $codes2Urls[$code];
        } elseif ($code && empty($url)) {
            $code = strtolower($code);
            if (!isset($lowerCodes[$code])) {
                return null;
            }
            $code = $lowerCodes[$code];
            $url = $codes2Urls[$code];
        } else {
            return null;
        }

        // Fill icon only when useful.
        if ($output === 'icon') {
            return $this->rightsStatementsIcon($code, $options);
        }

        $outputData = array_replace([
            'code' => null,
            'uri' => str_replace('https:', 'http:', $url),
            'url' => null,
            'label' => null,
            'title' => null,
            'summary' => null,
            'icon' => null,
        ], $data[$code]);
        if (array_key_exists($output, $outputData)) {
            return $outputData[$output];
        }
        $outputData['icon'] = $this->rightsStatementsIcon($code, $options);
        return $outputData;
    }

    /**
     * Get the html code from a Rights Statements uri (recommended) or code.
     *
     * @link https://rightsstatements.org/en/documentation/assets.html.
     * @link https://rightsstatements.org/en/documentation/usage_guidelines.html
     */
    public function rightsStatementsIcon(
        string $code,
        array $options = []
    ): ?string {
        $defaultOptions = [
            'icon_type' =>  'button',
            'format' => 'png',
            'as_image' => false,
            'color'  => 'dark-white-interior',
        ];
        $options += $defaultOptions;

        $codes2Urls = [
            'InC' => 'https://rightsstatements.org/vocab/InC/1.0/',
            'InC-RUU' => 'https://rightsstatements.org/vocab/InC-RUU/1.0/',
            'InC-NC' => 'https://rightsstatements.org/vocab/InC-NC/1.0/',
            'InC-EDU' => 'https://rightsstatements.org/vocab/InC-EDU/1.0/',
            'InC-OW-EU' => 'https://rightsstatements.org/vocab/InC-OW-EU/1.0/',
            'NoC-OKLR' => 'https://rightsstatements.org/vocab/NoC-OKLR/1.0/',
            'NoC-CR' => 'https://rightsstatements.org/vocab/NoC-CR/1.0/',
            'NoC-NC' => 'https://rightsstatements.org/vocab/NoC-NC/1.0/',
            'NoC-US' => 'https://rightsstatements.org/vocab/NoC-US/1.0/',
            'NKC' => 'https://rightsstatements.org/vocab/NKC/1.0/',
            'UND' => 'https://rightsstatements.org/vocab/UND/1.0/',
            'CNE' => 'https://rightsstatements.org/vocab/CNE/1.0/',
        ];
        $lowerCodes = array_change_key_case(array_combine(array_keys($codes2Urls), array_keys($codes2Urls)));

        $lowerCode = strtolower($code);
        if (!isset($lowerCodes[$lowerCode])) {
            return null;
        }
        $code = $lowerCodes[$lowerCode];

        $codes2Labels = [
            'InC' => 'In Copyright',
            'InC-RUU' => 'In Copyright - Rights-holder(s) Unlocatable or Unidentifiable',
            'InC-NC' => 'In Copyright - Non-Commercial Use Permitted',
            'InC-EDU' => 'In Copyright - Educational Use Permitted',
            'InC-OW-EU' => 'In Copyright - EU Orphan Work',
            'NoC-OKLR' => 'No Copyright - Other Known Legal Restrictions',
            'NoC-CR' => 'No Copyright - Contractual Restrictions',
            'NoC-NC' => 'No Copyright - Non-Commercial Use Only',
            'NoC-US' => 'No Copyright - United States',
            'NKC' => 'No Known Copyright',
            'UND' => 'Copyright Undetermined',
            'CNE' => 'Copyright Not Evaluated',
        ];

        $codes2icons = [
            'InC' => 'InC',
            'InC-RUU' => 'InC-UNKNOWN',
            'InC-NC' => 'InC-NONCOMMERCIAL',
            'InC-EDU' => 'InC-EDUCATIONAL',
            'InC-OW-EU' => 'InC-EU-ORPHAN',
            'NoC-OKLR' => 'NoC-OTHER',
            'NoC-CR' => 'NoC-CONTRACTUAL',
            'NoC-NC' => 'NoC-NONCOMMERCIAL',
            'NoC-US' => 'NoC-US',
            'NKC' => 'Other-UNKNOWN',
            'UND' => 'Other-UNDETERMINED',
            'CNE' => 'Other-NOTEVALUATED',
        ];

        $url = $codes2Urls[$code];
        $label = $codes2Labels[$code];
        $format = strtolower((string) $options['format']) === 'svg' ? 'svg' : 'png';

        $iconTypes = [
            'button' => 'buttons',
            'buttons' => 'buttons',
            'icon' => 'icons',
            'icons' => 'icons',
        ];
        $iconType = $iconTypes[$options['icon_type']] ?? 'buttons';
        if ($iconType === 'icons') {
            $colors = [
                'dark',
                'dark-white-interior',
                'white',
            ];
            $filename = strtok($codes2icons[$code], '-') . '.Icon-Only';
        } else {
            $colors = [
                'dark',
                'dark-white-interior',
                'dark-white-interior-blue-type',
                'white',
            ];
            $filename = $codes2icons[$code];
        }

        $color = in_array($options['color'], $colors) ? $options['color'] : 'dark-white-interior';

        $assetUrl = $this->view->assetUrl("img/rights-statements/$iconType/$format/$filename.$color.$format");
        return $options['as_image']
            ? sprintf('<img src="%s" alt="%s"/>', $assetUrl, $label)
            : sprintf('<a href="%s" alt="%s" _target="blank"><img src="%s" alt="%s"/></a>', $url, $label, $assetUrl, $label);
    }

    /**
     * Get the standard Creative Commons code from a uri (recommended) or a
     * string (code "cc-xx-xx" or text).
     *
     * Support old uris and codes, versionned and ported or not.
     * Fix some inputs too and manage public domain.
     *
     * Note: the official uris use scheme "http:", not "https:". For a simpler
     * web browsing, the https is used in the output (url, not uri).
     *
     * @link https://creativecommons.org/ns
     * @link https://creativecommons.org/schema.rdf
     * @link https://creativecommons.org/publicdomain/mark/1.0/rdf
     * @link https://creativecommons.org/licenses/by-nc-sa/3.0/fr/legalcode
     *
     * @todo Check the license (some does not exists: missing "by", etc).
     */
    public function creativeCommons(
        ?AbstractResourceEntityRepresentation $resource,
        ?string $term = 'dcterms:license',
        ?string $codeOrUrl = null,
        ?string $output = null,
        array $options = []
    ): ?array {
        // This table is available as rdf.
        // Other codes are not standard and not supported.
        $data = [
            'publicdomain' => [
                'cc' => 'public domain',
                'code' => 'publicdomain',
                'url' => 'https://creativecommons.org/publicdomain/mark/1.0/',
                'label' => 'Public Domain',
                'title' => 'Pas de droit d’auteur',
                'summary' => 'Cette œuvre a été identifiée comme libre de restrictions connues selon les lois du droit d’auteur, y compris tous les droits affiliés et connexes. Vous pouvez copier, modifier, distribuer et jouer l’œuvre, même à des fins commerciales, sans avoir besoin d’une permission.',
            ],
            'cc0' => [
                'cc' => 'cc0',
                'code' => 'cc0',
                'url' => 'https://creativecommons.org/publicdomain/zero/1.0/',
                'label' => 'Public Domain Dedication', // 'CC0 1.0 Universal',
                'title' => 'Transfert dans le domaine public',
                'summary' => 'La personne qui a associé une œuvre à cet acte a dédié l’œuvre au domaine public en renonçant dans le monde entier à ses droits sur l’œuvre selon les lois sur le droit d’auteur, droit voisin et connexes, dans la mesure permise par la loi. Vous pouvez copier, modifier, distribuer et représenter l’œuvre, même à des fins commerciales, sans avoir besoin de demander l’autorisation.',
            ],
            'by' => [
                'cc' => 'cc-by',
                'code' => 'by',
                'url' => 'https://creativecommons.org/licenses/by/4.0/',
                'label' => 'Attribution',
                'title' => 'Attribution',
                'summary' => 'Vous devez créditer l’Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l’Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l’Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre.',
            ],
            'by-sa' => [
                'cc' => 'cc-by-sa',
                'code' => 'by-sa',
                'url' => 'https://creativecommons.org/licenses/by-sa/4.0/',
                'label' => 'Attribution-ShareAlike',
                'title' => 'Attribution - Partage dans les mêmes conditions',
                'summary' => null,
            ],
            'by-nc' => [
                'cc' => 'cc-by-nc',
                'code' => 'by-nc',
                'url' => 'https://creativecommons.org/licenses/by-nc/4.0/',
                'label' => 'Attribution-NonCommercial',
                'title' => 'Attribution - Pas d’utilisation commerciale',
                'summary' => null,
            ],
            'by-nc-sa' => [
                'cc' => 'cc-nc-sa',
                'code' => 'by-nc-sa',
                'url' => 'https://creativecommons.org/licenses/by-nc-sa/4.0/',
                'label' => 'Attribution-NonCommercial-ShareAlike',
                'title' => 'Attribution - Pas d’utilisation commerciale - Partage dans les mêmes conditions',
                'summary' => null,
            ],
            'by-nd' => [
                'cc' => 'cc-by-nd',
                'code' => 'by-nd',
                'url' => 'https://creativecommons.org/licenses/by-nd/4.0/',
                'label' => 'Attribution-NoDerivatives',
                'title' => 'Attribution - Pas de modifications',
                'summary' => null,
            ],
            'by-nc-nd' => [
                'cc' => 'cc-by-nc-nd',
                'code' => 'by-nc-nd',
                'url' => 'https://creativecommons.org/licenses/by-nc-nd/4.0/',
                'label' => 'Attribution-NonCommercial-NoDerivatives',
                'title' => 'Attribution - Pas d’utilisation commerciale - Pas de modifications',
                'summary' => null,
            ],
        ];

        $codes2Urls = array_column($data, 'url', 'code');
        $codes2Labels = array_column($data, 'label', 'code');
        $codes2LabelsLower = array_map('strtolower', $codes2Labels);

        $codes2UrlsNoVersions = [
            'publicdomain' => 'https://creativecommons.org/publicdomain/mark',
            'cc0' => 'https://creativecommons.org/publicdomain/zero',
            'by' => 'https://creativecommons.org/licenses/by',
            'by-sa' => 'https://creativecommons.org/licenses/by-sa',
            'by-nc' => 'https://creativecommons.org/licenses/by-nc',
            'by-nc-sa' => 'https://creativecommons.org/licenses/by-nc-sa',
            'by-nd' => 'https://creativecommons.org/licenses/by-nd',
            'by-nc-nd' => 'https://creativecommons.org/licenses/by-nc-nd',
        ];

        $url = null;
        $code = null;
        $label = null;

        $translate = $this->view->plugin('translate');
        $publicDomain = $translate('public domain');
        $publicDomainDedication = $translate('public domain dedication');

        $checkCode = function ($input) use ($codes2Urls, $codes2Labels, $codes2LabelsLower, $publicDomain, $publicDomainDedication): ?string {
            $input = trim((string) $input);
            if (!strlen($input)) {
                return null;
            }
            // Clean string to simplify search..
            $input = str_replace('http://', 'https://', strtok($input, '?'));
            $inputLower = mb_strtolower($input);
            $value = strtok($inputLower, ' ');
            // Common names not in list.
            if (in_array($inputLower, [
                'public domain', 'pd', 'public-domain',
                'domain public', 'domain-public', 'domainpublic',
                'domaine public', 'domaine-public', 'domainepublic',
                $publicDomain
            ])) {
                return 'publicdomain';
            }
            if (in_array($inputLower, [
                'public domain dedication', 'cc0', 'zero', 'cc-zero',
                $publicDomainDedication
            ])) {
                return 'cc0';
            }
            // Check for urls.
            $code = array_search($value, $codes2Urls);
            if ($code) {
                return $code;
            }
            // Check for labels.
            $code = array_search($input, $codes2Labels);
            if ($code) {
                return $code;
            }
            $code = array_search($inputLower, $codes2LabelsLower);
            if ($code) {
                return $code;
            }
            // Check for codes without "cc-" or "cc ".
            $code = mb_substr($inputLower, 3);
            if (isset($codes2Urls[$code])) {
                return $code;
            }
            $code = $value;
            if (isset($codes2Urls[$code])) {
                return $code;
            }
            $code = $inputLower;
            if (isset($codes2Urls[$code])) {
                return $code;
            }
            return null;
        };

        if ($resource) {
            foreach ($resource->value($term, ['all' => true]) as $value) {
                $url = strtolower((string) $value->uri());
                $label = trim((string) $value->value());
                $code = null;
                if ($url) {
                    $url = str_replace('http:', 'https:', strtok($url, '?'));
                    if (substr($url, 0, 28) === 'https://creativecommons.org/') {
                        break;
                    }
                    continue;
                }
                $labelHttps = str_replace('http:', 'https:', strtok($label, '?'));
                if (substr($labelHttps, 0, 28) === 'https://creativecommons.org/') {
                    $url = $labelHttps;
                    $label = null;
                    break;
                }
                $code = $checkCode($label);
                if ($code) {
                    break;
                }
            }
            if (empty($url) && empty($code)) {
                return null;
            }
        } elseif ($codeOrUrl) {
            $codeOrUrl = trim(strtolower(str_replace('http:', 'https:', strtok($codeOrUrl, '?'))));
            if (substr($codeOrUrl, 0, 28) === 'https://creativecommons.org/') {
                $url = $codeOrUrl;
            } else {
                $code = $checkCode($codeOrUrl);
                if (!$code) {
                    return null;
                }
            }
        } else {
            return null;
        }

        if ($url && $code) {
            $code = $checkCode($code) ?? $code;
        } elseif ($url && empty($code)) {
            // Manage version, juridiction port, and missing "/".
            $regex = '~^(.*?)(?:/?|/\d\.\d/?|/\d\.\d/[a-z][a-z][a-z]?/?)$~';
            $noVersionUrl = preg_replace($regex, '$1', strtok($url, '?'));
            $code = array_search(str_replace('http:', 'https:', $noVersionUrl), $codes2UrlsNoVersions);
            if (!$code) {
                // Support some common direct codes in values.
                $fixes = [
                    'pd' => 'publicdomain',
                    'mark' => 'publicdomain',
                    'zero' => 'cc0',
                ];
                // Ordered codes by length, except "publicdomain".
                $orderedCodes = ['mark', 'zero', 'publicdomain', 'by-nc-nd', 'by-nc-sa', 'by-nc', 'by-nd', 'by-sa', 'cc0', 'by', 'pd'];
                foreach ($orderedCodes as $orderedCode) {
                    if (strpos($url, $orderedCode)) {
                        $code = $fixes[$orderedCode] ?? $orderedCode;
                        break;
                    }
                }
                if (!$code) {
                    return null;
                }
            }
        } elseif ($code && empty($url)) {
            $code = $checkCode($code);
            if (!isset($codes2Urls[$code])) {
                return null;
            }
            $url = $codes2Urls[$code];
        } else {
            return null;
        }

        // Fill icon only when useful.
        if ($output === 'icon') {
            return $this->creativeCommonsIcon($code, $options);
        }

        if (($output === 'summary' || !isset($data[$code][$output]))
            && empty($data[$code]['summary'])
        ) {
            foreach (explode('-', $code) as $codePart) {
                $data[$code]['summary'] .= $this->creativeCommonsPart($codePart)['summary'] . "\n";
            }
            $data[$code]['summary'] = trim($data[$code]['summary']);
        }

        $outputData = array_replace([
            'code' => null,
            'uri' => str_replace('https:', 'http:', $url),
            'url' => null,
            'label' => null,
            'title' => null,
            'summary' => null,
            'icon' => null,
        ], $data[$code]);
        if (array_key_exists($output, $outputData)) {
            return $outputData[$output];
        }
        $outputData['icon'] = $this->creativeCommonsIcon($code, $options);
        return $outputData;
    }

    /**
     * Get the html code from a Creative Commons uri (recommended) or code ("cc-xx-xx").
     *
     * Icons are available on http://i.creativecommons.org/l/by-nc-sa/3.0/fr/88x31.png,
     * that redirects to https://licensebuttons.net/l/by-nc-sa/3.0/fr/88x31.png etc.
     *
     * @link https://licensebuttons.net/i/
     * @link https://creativecommons.org/about/downloads/
     */
    public function creativeCommonsIcon(
        string $code,
        array $options = []
    ): ?string {
        $defaultOptions = [
            'icon_type' => 'button',
            'format' => 'png',
            'as_image' => false,
            'currency' => 'dollar',
        ];
        $options += $defaultOptions;

        // Other codes are not standard and not supported.
        $codes2Urls = [
            'publicdomain' => 'https://creativecommons.org/publicdomain/mark/1.0/',
            'cc0' => 'https://creativecommons.org/publicdomain/zero/1.0/',
            'by' => 'https://creativecommons.org/licenses/by/4.0/',
            'by-sa' => 'https://creativecommons.org/licenses/by-sa/4.0/',
            'by-nc' => 'https://creativecommons.org/licenses/by-nc/4.0/',
            'by-nc-sa' => 'https://creativecommons.org/licenses/by-nc-sa/4.0/',
            'by-nd' => 'https://creativecommons.org/licenses/by-nd/4.0/',
            'by-nc-nd' => 'https://creativecommons.org/licenses/by-nc-nd/4.0/',
        ];
        if (!isset($codes2Urls[$code])) {
            return null;
        }

        $codes2Labels = [
            'publicdomain' => 'Public Domain',
            'cc0' => 'Public Domain Dedication', // 'CC0 1.0 Universal',
            'by' => 'Attribution',
            'by-sa' => 'Attribution-ShareAlike',
            'by-nc' => 'Attribution-NonCommercial',
            'by-nc-sa' => 'Attribution-NonCommercial-ShareAlike',
            'by-nd' => 'Attribution-NoDerivatives',
            'by-nc-nd' => 'Attribution-NonCommercial-NoDerivatives',
        ];

        $iconTypes = [
            'buttons' => '88x31',
            'button' => '88x31',
            'normal' => '88x31',
            'compact' => '80x15',
            'icon' => 'icons',
            '88x31' => '88x31',
            '80x15' => '80x15',
            'icons' => 'icons',
        ];

        $iconType = $iconTypes[$options['icon_type']] ?? '88x31';
        $format = strtolower((string) $options['format']) === 'svg' ? 'svg' : 'png';
        $translate = $this->view->plugin('translate');

        $url = $codes2Urls[$code];
        $label = $code === 'publicdomain'
            ? $translate('Public domain') // @translate
            : 'Creative Commons ' . $codes2Labels[$code];
        $html = $options['as_image']
            ? ''
            : sprintf('<a href="%s" _target="blank" rel="license noopener noreferrer" alt="%s">', $url, $label);

        if ($iconType === 'icons') {
            $codes2Parts = [
                'cc' => 'Creative Commons',
                'publicdomain' => 'Public Domain',
                'cc0' => 'Public Domain Dedication',
                'by' => 'Attribution',
                'sa' => 'ShareAlike',
                'nc' => 'NonCommercial',
                'nd' => 'NoDerivatives',
            ];

            $codes2icons = [
                'cc' => 'cc',
                // For public domain, icon is "pd" (deprecated "public-domain").
                'publicdomain' => 'pd',
                'public-domain' => 'pd',
                'pd' => 'pd',
                // For cc0, the part icon is "zero".
                'cc0' => 'zero',
                'zero' => 'zero',
                'by' => 'by',
                'sa' => 'sa',
                'nc' => 'nc',
                'nc-dollar' => 'nc',
                'nc-euro' => 'nc-eu',
                'nc-yen' => 'nc-jp',
                'nc-eu' => 'nc-eu',
                'nc-jp' => 'nc-jp',
                'nd' => 'nd',
            ];

            $html .= '<span>';
            $baseAsset = $this->view->assetUrl('img/creative-commons/icons/');
            $codes = array_filter(explode('-', $code));
            if (reset($codes) !== 'cc') {
                array_unshift($codes, 'cc');
            }
            foreach ($codes as $part) {
                if (!isset($codes2Parts[$part])) {
                    continue;
                }
                $partIcon = $part === 'nc' ? 'nc-' . $options['currency'] : $part;
                $html .= sprintf('<img src="%s" alt="%s"/>', $baseAsset . $codes2icons[$partIcon] . $format, $codes2Parts[$part]);
            }
            return $html
               . '</span>'
                . ($options['as_image'] ? '' : '</a>');
        }

        $currency = in_array($options['currency'], ['euro', 'eu']) && in_array($code, ['by-nc', 'by-nc-sa', 'by-nc-nd'])
            ? '.eu'
            : '';
        $filenames = [
            'publicdomain' => 'public-domain',
            'pd' => 'public-domain',
            'mark' => 'public-domain',
            'cc0' => 'cc-zero',
            'zero' => 'cc-zero',
        ];
        $filename = $filenames[$code] ?? $code;
        $assetUrl = $this->view->assetUrl("img/creative-commons/$iconType/$filename$currency.$format");
        return $html
            . sprintf('<img src="%s" alt="%s"/>', $assetUrl, $label)
            . ($options['as_image'] ? '' : '</a>');
}

    public function creativeCommonsPart(string $code): array
    {
        $data = [
            'cc' => [
                'label' => 'Licence Creative Commons',
                'title' => 'Licence Creative Commons',
                'summary' => 'Les licences de droits d’auteur et les outils Creative Commons apportent un équilibre à l’intérieur du cadre traditionnel “tous droits réservés” créé par les lois sur le droit d’auteur. Nos outils donnent à tout le monde, du créateur individuel aux grandes entreprises et aux institutions publiques, des moyens simples standardisés d’accorder des permissions de droits d’auteur supplémentaires à leurs œuvres. La combinaison de nos outils et de nos utilisateurs est un fonds commun numérique vaste et en expansion, un espace commun de contenus pouvant être copiés, distribués, modifiés, remixés, et adaptés, le tout dans le cadre des lois sur le droit d’auteur.',
            ],
            'publicdomain' => [
                'label' => 'Public Domain',
                'title' => 'Pas de droit d’auteur',
                'summary' => 'Cette œuvre a été identifiée comme libre de restrictions connues selon les lois du droit d’auteur, y compris tous les droits affiliés et connexes. Vous pouvez copier, modifier, distribuer et jouer l’œuvre, même à des fins commerciales, sans avoir besoin d’une permission.',
            ],
            'cc0' => [
                'label' => 'Public Domain Dedication',
                'title' => 'Transfert dans le domaine public',
                'summary' => 'La personne qui a associé une œuvre à cet acte a dédié l’œuvre au domaine public en renonçant dans le monde entier à ses droits sur l’œuvre selon les lois sur le droit d’auteur, droit voisin et connexes, dans la mesure permise par la loi. Vous pouvez copier, modifier, distribuer et représenter l’œuvre, même à des fins commerciales, sans avoir besoin de demander l’autorisation.',
            ],
            'by' => [
                'label' => 'Attribution',
                'title' => 'Attribution',
                'summary' => 'Vous devez créditer l’Œuvre, intégrer un lien vers la licence et indiquer si des modifications ont été effectuées à l’Œuvre. Vous devez indiquer ces informations par tous les moyens raisonnables, sans toutefois suggérer que l’Offrant vous soutient ou soutient la façon dont vous avez utilisé son Œuvre.',
            ],
            'sa' => [
                'label' => 'Share Alike',
                'title' => 'Partage dans les mêmes conditions',
                'summary' => 'Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l’Œuvre originale, vous devez diffuser l’Œuvre modifiée dans les même conditions, c’est-à-dire avec la même licence avec laquelle l’Œuvre originale a été diffusée.',
            ],
            'nc' => [
                'label' => 'Non Commercial',
                'title' => 'Pas d’utilisation commerciale',
                'summary' => 'Vous n’êtes pas autorisé à faire un usage commercial de cette Œuvre, tout ou partie du matériel la composant.',
            ],
            'nd' => [
                'label' => 'Non derivatives',
                'title' => 'Pas de modifications',
                'summary' => 'Dans le cas où vous effectuez un remix, que vous transformez, ou créez à partir du matériel composant l’Œuvre originale, vous n’êtes pas autorisé à distribuer ou mettre à disposition l’Œuvre modifiée.',
            ],
        ];
        return $data[$code] ?? [];
    }
}
